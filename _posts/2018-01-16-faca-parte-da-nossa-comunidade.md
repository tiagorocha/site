---
layout: post
title:  "Faça parte da nossa comunidade"
date:   2018-01-16 00:00:00 -0300
description:
categories: comunidade mulheres
author: Admin
icon: female
color: default
---

A Debian Brasília convida todas as mulheres a participarem da nossa comunidade. Aqui somos todos iguais, respeitamos as opiniões e opções de cada um e queremos você com a gente.

A tecnologia é fascinante, porem poucas mulheres parecem se interessar pelo assunto, então se você gosta de tecnologia, gosta de software livre e quer participar com a gente, basta entrar em contado através do e-mail `contato@debianbrasilia.org`.