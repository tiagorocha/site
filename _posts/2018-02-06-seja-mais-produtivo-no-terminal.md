---
layout: post
title:  Seja mais produtivo no terminal
date:   2018-02-06 00:00:00 -0300
description: Se você é um usuário GNU/Linux de longa data, provavelmente usa o terminal com regularidade, e já descobriu que...
categories: terminal
author: David Polverari
icon: terminal
color: default
---

#### Introdução
Se você é um usuário GNU/Linux de longa data, provavelmente usa o terminal com regularidade, e já descobriu que a maioria das tarefas é realizada muito mais rapidamente nele do que utilizando uma GUI. Ainda assim, quando você está terminando de digitar um comando longo e tem que corrigir uma das palavras bem no meio dele, à medida que seus dedos procuram a seta da esquerda para poder alcançar a palavra malcriada, você se pergunta: não há uma maneira melhor de fazer isso?

GNU Readline ao resgate!
A solução está em uma biblioteca que é amplamente utilizada por diversas aplicações no mundo *NIX: a GNU Readline. Esta biblioteca, como seu nome diz, fornece um conjunto de funções que permitem ao usuário editar linhas de comando à medida que elas são digitadas, bem como outros recursos, tais como a manutenção de um histórico das linhas digitadas e sua navegação. Desse modo, os atalhos que você usa no bash serão igualmente úteis quando estiver no prompt do MySQL!

#### Experimente os atalhos abaixo para agilizar sua vida no terminal:

`Ctrl-L`: limpa o terminal atual, mantendo a linha digitada até o momento.
`Ctrl-A`: desloca o cursor para o início da linha.
`Ctrl-E`: desloca o cursor para o final da linha.
`Ctrl-W`: apaga a palavra anterior.
`Ctrl-K`: apaga do cursor até o final da linha.
`Ctrl-U`: apaga do cursor até o início da linha.
`Ctrl-Y`: cola o texto apagado anteriormente na posição atual do cursor.
`Ctrl-/`: desfaz (undo) a ação anterior.

`Alt-B`: volta uma palavra.
`Alt-F`: avança uma palavra.
`Alt-D`: apaga a palavra atual.
`Alt-C`: capitaliza a palavra atual.
`Alt-L`: converte a palavra atual para minúsculas.
`Alt-U`: converte a palavra atual para maiúsculas.
`Alt-T`: troca a palavra atual pela anterior.

No próximo artigo, iremos explorar como navegar eficientemente pelo histórico dos comandos digitados. Até lá!