FROM ruby:2.7-alpine

RUN apk add --no-cache build-base gcc bash cmake git

WORKDIR /site

ADD Gemfile .
ADD Gemfile.lock .

RUN gem install bundler && bundle

EXPOSE 4000

COPY . .

ENTRYPOINT [ "jekyll" ]

CMD [ "serve" ]